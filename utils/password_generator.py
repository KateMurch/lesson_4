import string
import secrets


class PasswordGenerator:
    def __init__(self, length):
        self.length = length

    def __get_alphabet(self):
        return string.ascii_letters + string.digits + string.punctuation

    @property
    def alphabet(self):
        return self.__get_alphabet()

    def __generate_password(self):
        if self.length:
            return ''.join(secrets.choice(self.alphabet) for _ in range(self.length))
        return ''

    @property
    def password(self):
        return self.__generate_password()

