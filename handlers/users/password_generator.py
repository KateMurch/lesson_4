from aiogram import types
from aiogram.dispatcher.filters import Regexp
import requests

from loader import dp
from utils import PasswordGenerator


@dp.message_handler()  # (Regexp(r'[0-9]+'))
async def pw_generator(message: types.Message):
    # length = int(message.text)
    # pw = PasswordGenerator(length)
    # await message.answer(pw.password)
    p = requests.get('https://random.dog/woof.json')
    await message.answer(p.json()["url"])

